package main

import (
	"fmt"
	"net/http"
)

func mainPage(w http.ResponseWriter, req *http.Request){
	fmt.Fprintf(w, "hello there!!\n")
}

func main(){
	http.HandleFunc("/mainPage", mainPage)
	http.ListenAndServe(":8080", nil)
}